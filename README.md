# README #

### What is this repository for? ###

This repository contains the code for fast camera localization using the algorithms described in:

Raúl Díaz, Charless C. Fowlkes. 
"Cluster-Wise Ratio Tests for Fast Camera Localization"
arXiv preprint: https://arxiv.org/abs/1612.01689


### Requirements ###

* FLANN with Matlab bindings [https://github.com/mariusmuja/flann]

### How do I get set up? ###

You will need:

* A Structure-from-Motion (SfM) model, in bundler format [https://github.com/snavely/bundler_sfm].
* A binary file containing all descriptors observed in the SfM model, listed in the same order as they appear in the bundle.out file.
* A set of test images alongside their descriptors.

### How do I use the code? ###

* Obtain a camera pose clustering
* Compute the intra-cluster nearest neighbors Vnn (see computeVnn.m)
* Run localizeMatches.m, adapting paths to the data appropriately
* Run localizeImagesBatch.m to find the full 6DOF camera pose solution

### Dataset

The Eng-Quad dataset presented in the paper can be found here:
http://vision.ics.uci.edu/datasets/index.html

Please note that the dataset is scaled in feet. Please use a 0.3048 m/ft conversion for benchmarks in meters!

### Who do I talk to? ###

* Please send a message to Raúl Díaz (rdiazgar@uci.edu) for questions and comments.

### Copyright (C) 2017 Raúl Díaz, Charless C. Fowlkes ###

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
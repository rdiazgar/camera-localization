function [C,T] = bundlerconnectivitygraph(bundler)

v = sparse(bundler.visibility(:,1),bundler.visibility(:,2),1, ...
           numel(bundler.cameras),size(bundler.points,1));
       
C = v*v';

if nargout>1
    T = v'*v;
end

return;

%% script to localize putative matches

% load dependencies
addpath ~/software/flann/src/matlab/
addpath ../lib/utils
addpath ../lib/flann

siftfile = '~/databases/SF/SF-0/sf_0.sift.bin';

% load test list
test_images = importdata('~/databases/SF/SF-0/list_query_sf_with_intrinsics.txt');
test_images = test_images.textdata;
bfolder = '~/databases/SF/SF-0/sf_query'; % base folder of image paths

% Load bundler model (or using parsebundler function)
display('Loading SfM data');
if ~exist('bundler','var')
      load ~/databases/SF/SF-0/sf_0.GPS_aligned.correct_rotation.out.mat
end
% read 3D point sift
display('Loading SIFT data');
if ~exist('DESC','var')
      load ~/databases/SF/SF-0/sf_0.sift.rootsift.mat
%if ~exist('DESC','var')
%     f = fopen(siftfile);
%     DESC = fread(f);
%     DESC = uint8(reshape(DESC,[128 length(DESC)/128]));
%     DESC = rootsift(DESC);
%     fclose(f);
end
 
% create kd-tree (or load one if it's precomputed)
display('Creating kd-tree');
if ~exist('fwdtree','var')
    options.algorithm = 'kdtree';
    options.trees = 1;
    [fwdtree, options] = flann_build_index(DESC,options);
    options.checks = 128;
end
bckoptions.algorithm = 'kdtree';
bckoptions.trees = 1;
bckoptions.checks = 128;

% Load histogram of assigned views
display('Loading Vnn Data');
if ~exist('Vnn','var')
   load ~/databases/SF/SF-0/sf_0.vnn.rootsift.mat
   Vnn = Vnn(:);
   binIdx = binIdx(:);
   Ksp = numel(bundler.cameras); % number of clusters/images
end
% search params
ratio = (0.7)^2;
n = 8; % so that 5NN might pass the global ratio test
minMatches = 500; % N_F in paper
minBackMatches = 300; % N_B in paper


%% loop over images
display('Localizing...');
for i=770:numel(test_images)
 
    display(sprintf('Image %u',i));

    % get file names for test image and key file
    ifile = strcat(bfolder, '/', test_images{i}); 
    sfile = strrep(ifile,'jpg','key');  
    [FEATURES, ~] = readsift(sfile);
    FEATURES = rootsift(FEATURES);
    [bcktree, bckoptions] = flann_build_index(FEATURES,bckoptions);

    % forward match
    idx = randperm(size(FEATURES,2));%,min(500,size(FEATURES,2)));
    IP_TMP = [];
    D_TMP = [];
    forward_time = 0;

    display('forward');
    while size(IP_TMP,1)<minMatches && ~isempty(idx)

        chunk = idx(1:min(100,numel(idx)));
        idx(1:numel(chunk)) = [];

        % 0 - retrieve the matches
        tic
        [INDEX_PAIRS, D] = matchDescriptors(DESC,FEATURES(:,chunk),n,'left',options, fwdtree);
        forward_time = forward_time + toc;

        if isempty(INDEX_PAIRS)
            continue
        end

        INDEX_PAIRS(:,2) = chunk(INDEX_PAIRS(:,2));

        % 1 - do a global ratio test prune 
        D = reshape(D,n,numel(chunk));
        Dratio = D(1,:)./D(end,:); %(D./(repmat(D(end,:),[size(D,1) 1])));
        Dratio = [repmat(Dratio,[n-1 1]); ones(1,size(D,2))];
        Dratio = Dratio(:);
        INDEX_PAIRS = INDEX_PAIRS(Dratio<=ratio,:);
        D = D(Dratio<=ratio);

        IP_TMP = cat(1,IP_TMP,INDEX_PAIRS);
        D_TMP = cat(1,D_TMP,D);

    end

    INDEX_PAIRS = IP_TMP;
    D = D_TMP;

    display('vote');
    % cluster-wise ratio test
    tic
    [votes, idVotes] = cwrt(INDEX_PAIRS,D,Vnn,binIdx,bundler.visibility(:,2),ratio);
    vote_time = toc;
    IP = INDEX_PAIRS(idVotes,:);


    % back match on the top k bins using a prioritization on the 
    % images to be backmatched.

    already_voted = false(size(votes));
    IP_final = [];
    voteMap = sparse(bundler.visibility(IP(:,1),1), ...
                     bundler.visibility(IP(:,1),2), ...
                     1,Ksp,size(bundler.points,1));
    orig_votes = votes;

    display('back');
    tic
    while any(votes) && sum(already_voted)<50

        % 1 - select next best view
        [~, next] = max(votes .* ~already_voted);
        putativeIdx = find(binIdx == next);

        % 2 - back-match
        IPbb = matchDescriptors(DESC(:,putativeIdx),FEATURES,ratio,'right',bckoptions,0,bcktree);
        IPbb(:,1) = putativeIdx(IPbb(:,1));
        already_voted(next) = true;

        IP_final = cat(1,IP_final,IPbb);

        if size(IP_final,1) >= minBackMatches
            break;
        end

        % 3 - update votes vector for newer backmatched votes
        tracksMatched = bundler.visibility(IPbb(:,1),2);
        viewsWthoseTracks = ismember(bundler.visibility(:,2),tracksMatched);
        voteMapbb = sparse(bundler.visibility(viewsWthoseTracks,1), ...
                         bundler.visibility(viewsWthoseTracks,2), ...
                         1,Ksp,size(bundler.points,1));
        voteMap = double(voteMap) | voteMapbb;
        if size(IPbb,1) > 11
          votes = full(sum(voteMap,2));
        end

    end
    back_time = toc;

    IPbb = IP_final;

    out = sprintf('~/databases/SF/SF-0/results_NN7_back50/%04u.mat',i);
    save(out,'votes','IP','IPbb','orig_votes', ...
         'forward_time','vote_time','back_time','ifile');


    flann_free_index(bcktree);

end

flann_free_index(fwdtree);

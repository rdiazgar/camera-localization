function [ M ] = P4Pfwrapper(X)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[f, R, t] = P4Pf(X(1:2,:), X(4:6,:));

if ~isempty(f)
    M.f = f;
    M.R = R;
    M.t = t;
else
    M = [];
end

end


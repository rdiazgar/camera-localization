function [votes, matchingVotes] = cwrt(INDEX_PAIRS, D, Vnn, view2bin, view2track, ratio)
% % Inputs
% INDEX_PAIRS - matches [view feature]
% D = sift distances
% Vnn - distance to the nearest track per every view within its bin.
% view2bin - lookup vector to relate views and histogram bins
% view2track - lookup to relate views and tracks (3D points)
% ratio - threshold to apply the ratio test

% 0 - Compute data structure
% [viewId featId trackId binId siftDist VnnDist]
data = [INDEX_PAIRS view2track(INDEX_PAIRS(:,1)) view2bin(INDEX_PAIRS(:,1)) D(:) Vnn(INDEX_PAIRS(:,1))];
[data, sortIdx] = sortrows(data,[4 2 5]);


% 1 - Make sure [featId pointId binId] is unique (guaranteing smallest sift dist)
% and removing any possible feature matching the same track twice.
[~,ia] = unique(data(:,[2 3 4]),'rows','stable'); 
data = data(ia,:);

% 2 - compute ratios - lrt=local ratio test ; vrt=closest view ratio test
lrt = (data(1:end-1,5)./data(2:end,5)); % compute ratio
lrt = cat(1,lrt,0); % last element is good unless proven wrong in vrt
vrt = data(:,5) ./ ( data(:,5) + data(:,6) );

% 3 - check if feature/bin is repeated more than once; 
% d = true if first feature in a sequence
% b = true if first feature of the bin
f = ~~diff(data(:,2)); % if true, first of a sequence
f = [true;f]; % first is always first in a sequence
b = ~~diff(data(:,4)); % first feature of a bin
b = [true;b]; % first is always first 
f(b) = true; % make first element of a bin always first


% 4 - determine if feature is fisrt in a sequence or a singleton match
firstFeat = f(1:end-1) & ~f(2:end); % first featId of many 
firstFeat = [firstFeat; false]; % last one is not first in sequence

singleton = f(1:end-1) & f(2:end); % singleton featIds
singleton = [singleton; f(end)];   % last one is singleton if d(end)==1


% 5 - assign corresponding ratio
r = zeros(size(ia));
r(firstFeat) = lrt(firstFeat); % assign local RT to repeated features
r(singleton) = vrt(singleton); % assign view RT to singleton features
r(~firstFeat & ~singleton) = inf; % set a high value to anybody else


% 6 - Compute final histogram count and indexes of original data to return
idx = r<=ratio;
votes = accumarray(data(:,4),idx);
if numel(votes) < max(view2bin)
    votes(max(view2bin)) = 0;
end
matchingVotes = sortIdx(ia(idx));


end

%% register a batch of images

addpath(genpath('../lib'));
addpath ../third-party/pkovesi/Robust/
addpath(genpath('../third-party/PnP_Toolbox/'));
addpath ../third-party/pkovesi/p4pf/

db_file = '~/code/ipfp-sac/lib/exif/cameraGenerated.txt';

test_images = importdata('~/databases/SF/SF-0/list_query_sf_with_intrinsics.txt');
focals = test_images.data(:,1);

folder = '~/databases/SF/SF-0/results_NN7_back50/';
files = dir([folder '*.mat']);
if ~exist('bundler', 'var')
   load ~/databases/SF/SF-0/sf_0.GPS_aligned.correct_rotation.out.mat
end


for i=1:numel(files)

    clear data;
    
    display(files(i).name);

    % load data needed for resection
    load([folder files(i).name]);

    % prepare putative data before RANSAC
    IP_put = IPbb;
    IP_put(:,1) = bundler.visibility(IP_put(:,1),2);
    IP_put = unique(IP_put,'rows');

    if size(IP_put,1) < 3
        continue
    end

%    [~, f_not] =  getCCDWidth(ifile,db_file);
    f_not = focals(i);
    
    sfile = strrep(ifile,'jpg','key');
    [~,POINTS] = readsift(sfile);
    POINTS = POINTS([2 1 3 4],:);
    x = POINTS(1:2,:)';

    data.x = x(IP_put(:,2),:);
    data.X = bundler.points(IP_put(:,1),:);
    data.path = ifile;

    % perform fine pose estimation

     if f_not && (size(IP_put,1) > 2)
         display('P3P');
         [cam_f, inliers_f,p3p_time] = localizeImage(data,6,f_not);
         display(sprintf('Inliers: %u',numel(inliers_f)));
         data.cam_fnot_p3p = cam_f;
         data.inliers_fnot_p3p = inliers_f;
     end
     
     if size(IP_put,1) > 3
     display('P4Pf');
     [cam_p4pf, inliers_p4pf, p4pf_time] = localizeImage(data,6);
     if ~isempty(cam_p4pf)
     	 data.cam_p4pf = cam_p4pf;
     	 data.inliers_p4pf = inliers_p4pf;
     end
     end
     
       
    % save the results
    save([folder files(i).name],'data','votes','IP','IPbb','orig_votes', ...
    'forward_time','vote_time','back_time','ifile','p3p_time','p4pf_time', ...
    'votes','IPbb');
end


function [ CCDWidth, focal ] = getCCDWidth(image_file, db_file)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    info = imfinfo(image_file);
    if nargin == 1
        switch info.DigitalCamera.FocalPlaneResolutionUnit
            case 1 
                FocalplaneUnits = 25.4;  % inch
                % According to the information I was using, 2 means meters.
                % But looking at the Cannon powershot's files, inches is the only
                % sensible value.
            case 2
                FocalplaneUnits = 25.4;
            case 3
                FocalplaneUnits = 10;     % centimeter
            case 4
                FocalplaneUnits = 1;      % millimeter
            case 5
                FocalplaneUnits = .001;   % micrometer
        end
        CCDWidth = max(info.DigitalCamera.CPixelXDimension)*FocalplaneUnits/info.DigitalCamera.FocalPlaneXResolution;
    else
        data = importdata(db_file);
        
        is_make = ismember({data.textdata{:,1}},strtrim(info.Make));
        is_model = ismember({data.textdata{:,2}},strtrim(info.Model));
        idx = find(is_make & is_model);
        if ~isempty(idx)
            CCDWidth = data.data(idx);
        else
            error('Could not find camera model. Please update database.')
        end
    end 
    
    if nargout == 2
        focal = max([info.Width info.Height])*info.DigitalCamera.FocalLength/CCDWidth;
    end
end



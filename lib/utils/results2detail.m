folder = '~/databases/SF/SF-0/results_NN7_back50/';
files = dir([folder '*.mat']);

if ~exist('bundler', 'var')
  load ~/databases/SF/SF-0/sf_0.GPS_aligned.correct_rotation.out.mat
end

qfolder = '~/databases/SF/SF-0/BuildingQueryImagesCartoIDCorrected/';
qfiles = dir([qfolder '*.jpg']);

out = [folder 'p3p_eff.detail'];
f = fopen(out,'w');
N = 50;

for i=1:numel(files)

    clear data;
    display(files(i).name);
    load([folder files(i).name]); 

    fprintf(f,'%s%s\n', qfolder, qfiles(i).name);
    idx = [];

    if exist('data','var') && isfield(data,'inliers_fnot_p3p')

     camIDS = bundler.visibility(IPbb(data.inliers_fnot_p3p,1),1);
     camCount = accumarray(camIDS,1);

      [idx, ~, count] = find(camCount);
      [count, sortIdx] = sort(count,'descend');
      idx = idx(sortIdx);

     siz = size(imread(ifile));
     map = zeros(siz(1:2));
     inliers = data.inliers_fnot_p3p;
     x = round(data.x(inliers,:));
     ind = sub2ind(siz(1:2),x(:,2),x(:,1));
     map(ind) = 1;
     SE = strel('rectangle',[12 12]);
     map = imdilate(map,SE);
     score = sum(~~map(:))/numel(SE.getnhood); 
     count(:) = score;
    %if sum(count) < 12
    %  continue;
    %end

     for j=1:numel(idx)
        fprintf(f,'%04u 0 %s\n', count(j), bundler.cameras(idx(j)).path);
     end

    end

    for j=numel(idx)+1:N
       fprintf(f,'0000 0 dummy\n');
    end

end

fclose(f);

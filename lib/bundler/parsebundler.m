function bundler = parsebundler(bundler_out, bundler_list)
%PARSEBUNDLER reads both output and file list files and creates a proper
%structure for further manipulation.
%INPUT: bundler_out -> bundler's output file path.
%       bundler_list -> bundler's filename list file path.
%       absolute -> 0 = bundler_list paths are relative (default). 1 =
%                   absolute paths
%OUTPUT: bundler: struct containing the following fields;
%           cameras -> cell array containing cameras
%               .f = focal length of the camera (in pixels)
%               .k = radial distortion
%               .t = camera translation
%               .position = camera position
%               .direction = vector indicating where camera points to
%               .R = camera rotation matrix
%               .c = camera center
%               .m = camera magnification
%               .path = full path of the image location
%            points -> list of 3D point position.                     
%            color  -> RGB color estimate of such point
%            visibility -> 5-column table containing the following visibility
%               information: [camera_id point_id feature_id x y]
%


fid = fopen(bundler_out);
textscan(fid,'%s %s %s %s \n',1);
n = textscan(fid,'%u %u \n',1);
n_cams = n{1};
n_points = n{2};

fib = fopen(bundler_list);
files = textscan(fib,'%s','delimiter','\n','BufSize',20000);
files = files{1};

path = fileparts(bundler_list);
paths = cellfun(@getP,files,'UniformOutput',0);
paths = cellfun(@(p) strcat(path,'/',p),paths,'UniformOutput',0);
fclose(fib);



% Camera processing %
display('Parsing cameras...');
parsed = textscan(fid,'%f %f %f \n',5*n_cams);
parsedcams = cell2mat(parsed);
f = parsedcams(1:5:end,1);
k = parsedcams(1:5:end,2:3);
parsedcams(1:5:end,:) = [];
t = parsedcams(4:4:end,:);
parsedcams(4:4:end,:) = [];

cellR = mat2cell(parsedcams,3*ones(1,n_cams));
cellt = mat2cell(t,ones(1,n_cams));

dets = cellfun(@(r) det(r),cellR);
rescue = 0;
if any(dets < 0)
    count = sum(dets < 0);
    warning(strcat('Some rotation matrices have negative determinants: ',num2str(count),'/',num2str(n_cams)));
    %rescue = 0;
end

%if rescue
%    cellR = cellfun(@(r,d) (r'*[1 0 0; 0 d 0; 0 0 1])',cellR,mat2cell(dets,ones(1,n_cams)),'UniformOutput',0);
    %position = cellfun(@(t,d) t.*[1 d 1],position,mat2cell(dets,ones(1,n_cams)),'UniformOutput',0);
    %direction = cellfun(@(t,d) t.*[1 d 1],direction,mat2cell(dets,ones(1,n_cams)),'UniformOutput',0);
    %cellt = cellfun(@(r,p) -(r'*p')',cellR,position,'UniformOutput',0);
%end

position = cellfun(@(r,t) (-r'*t')',cellR,cellt,'UniformOutput',0);
direction = cellfun(@(r) (r'*[0 0 -1]')', cellR,'UniformOutput',0);

cellf = mat2cell(f,ones(1,n_cams));
cellk = mat2cell(k,ones(1,n_cams));
cellid = num2cell(1:n_cams)';
cellc = mat2cell(zeros(n_cams,2),ones(1,n_cams));
cellm = mat2cell(ones(n_cams,2),ones(1,n_cams));


bundler.cameras = struct('f',cellf,'k',cellk,'t',cellt,'R',cellR, ...
    'position',position,'direction',direction,'path',paths,'id',cellid, ...
    'c',cellc,'m',cellm);


% Point processing %
display('Parsing points...');

parsed = textscan(fid,'%s','delimiter','\n','BufSize',20000);
fclose(fid);

parsed = parsed{1};
poi = parsed(1:3:end);
col = parsed(2:3:end);
vis = parsed(3:3:end);
clear parsed;

bundler.points = cellfun(@(x) sscanf(x,'%f'),poi,'UniformOutput',false);
bundler.points = cell2mat(bundler.points')';
bundler.colors = cellfun(@(x) sscanf(x,'%u'),col,'UniformOutput',false);
bundler.colors = cell2mat(bundler.colors')';
if rescue
    bundler.points = bundler.points*[1 0 0; 0 -1 0; 0 0 1];
end

idx = num2cell(1:double(n_points));
display('Processig tracks...');
pairs = cellfun(@getpairs,vis,idx','UniformOutput',0);

bundler.visibility = cell2mat(pairs);

% Additional functions
function pair = getpairs(str,idx)
    v =  sscanf(str,'%f');
    nv = v(1);
    vismat = reshape(v(2:end),[4 nv])';
    vispoint = repmat(idx,[nv 1]);
    
    pair = [vismat(:,1)+1 vispoint vismat(:,2)+1 vismat(:,3:4)];
end

function p = getP(str)
    a = strsplit(str,' ');
    p = a{1};
end

end


 

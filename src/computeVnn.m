%% compute, per every view, the nearest neighbor track within its corresponding bin
function computeVnn(bundler, siftfile, clusterfile)


%load(clusterfile);

%f = fopen(siftfile);
%DESC = fread(f);
%DESC = uint8(reshape(DESC,[128 length(DESC)/128]));
%DESC = rootsift(DESC);
%fclose(f);
load(siftfile);

%cam2bin = zeros(size(camIdx));
%cam2bin(camIdx) = cIdx;
%binIdx = cam2bin(bundler.visibility(:,1));
binIdx = bundler.visibility(:,1);


viewsPerBin = accumarray(binIdx(:),1:numel(binIdx),[],@(x) {x});
nonEmpty = ~cellfun(@isempty,viewsPerBin);
viewsPerBin = viewsPerBin(nonEmpty);

n = 50;

Vnn = zeros(size(binIdx));

for i=1:numel(viewsPerBin)
    display(num2str(i));
    
    if numel(viewsPerBin{i}) == 1
        continue
    end
    
    nV = numel(viewsPerBin{i});
    nNN = min([n nV]);
    
    success = false;
    while ~success
    try
     DESC_i = DESC(:,viewsPerBin{i});
     [IP, D] = matchDescriptors(DESC_i, DESC_i,nNN,'left');
                           
     tracks1 = bundler.visibility(viewsPerBin{i}(IP(:,1)),2);
     tracks2 = bundler.visibility(viewsPerBin{i}(IP(:,2)),2);
     
     tracks1 = reshape(tracks1,[nNN nV]);
     tracks2 = reshape(tracks2,[nNN nV]);

    catch
     continue;

    end
     success = true;
    end


    isDiffTrack = tracks1 ~= tracks2;
    D = reshape(D,[nNN nV]);

    [~, id] = max(isDiffTrack);
    
    idx = sub2ind(size(D),id,1:nV);
    Vnn(viewsPerBin{i}) = D(idx);
end

save(clusterfile, ...
    'binIdx', 'Vnn');%, 'cIdx', 'camIdx');


function [INDEX_PAIRS, DISTS] = matchDescriptors(FEATURES1, FEATURES2, ratio, method, options, kdtree1, kdtree2)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if nargin<7
    kdtree2 = 0;
end
if nargin<6
    kdtree1 = 0;
end
if nargin<5 || isempty(options)
 options.algorithm = 'kdtree';
 options.trees = 1;
 options.checks = 32;
end

if ratio <1
   nn = 2;
else
   nn = ratio;
   ratio = 0;
end

if nargout == 3
    nn = nn+1;
end

freek1 = 0;
freek2 = 0;

methods = {'left', 'right', 'intersect', 'union', 'back'};
if ~ismember(method,methods)
    error('wrong method parameter');
end
   

% Left image
if ismember(method,methods([1 3:5]))
    if ~kdtree1
        kdtree1 = flann_build_index(FEATURES1,options);
        freek1 = 1;
    end
    [im1_ids, dists2] = flann_search(kdtree1,FEATURES2,nn,options);
    [im2_ids] = meshgrid(1:size(FEATURES2,2),1:nn);
    
    if ~ratio 
            INDEX_PAIRSl = [im1_ids(:) im2_ids(:)];
            DISTSl = dists2(:);
    else
        good_matches = dists2(1,:) < dists2(2,:)*ratio;
        INDEX_PAIRSl = [im1_ids(1,good_matches)' im2_ids(1,good_matches)'];
        DISTSl = dists2(1,good_matches)';
    end
    if freek1
        flann_free_index(kdtree1);
    end
end

% Right image
if ismember(method,methods(2:5))
    if ~kdtree2
        kdtree2 = flann_build_index(FEATURES2,options);
        freek2 = 1;
    end
    
    if ismember(method,methods(5)) % back-matching
        [im2_ids, dists1] = flann_search(kdtree2,FEATURES1(:,INDEX_PAIRSl(:,1)),nn,options);
        [im1_ids] = meshgrid(1:size(INDEX_PAIRSl,1),1:nn);
        im1_ids = INDEX_PAIRSl(im1_ids,1);
    else
        [im2_ids, dists1] = flann_search(kdtree2,FEATURES1,nn,options);
        [im1_ids] = meshgrid(1:size(FEATURES1,2),1:nn);
    end

    if ~ratio
        INDEX_PAIRSr = [im1_ids(:) im2_ids(:)];
        DISTSr = dists1(:);
    else
        good_matches = dists1(1,:) < dists1(2,:)*ratio;
        INDEX_PAIRSr = [im1_ids(1,good_matches)' im2_ids(1,good_matches)'];
        DISTSr = dists1(1,good_matches)';
    end
    if freek2
        flann_free_index(kdtree2);
    end
end




if strcmp(method,'left')
    INDEX_PAIRS = INDEX_PAIRSl;
elseif strcmp(method,'right')
    INDEX_PAIRS = INDEX_PAIRSr;
elseif strcmp(method,'intersect') || strcmp(method,'back')
    [INDEX_PAIRS, iai, ibi] = intersect(INDEX_PAIRSr,INDEX_PAIRSl,'rows'); 
elseif strcmp(method,'union')
    %[INDEX_PAIRS, iau, ibu] = union(INDEX_PAIRSr,INDEX_PAIRSl,'rows','sorted'); 
end

if nargout > 1
    if strcmp(method,'left')
        DISTS = DISTSl;
    elseif strcmp(method,'right')
        DISTS = DISTSr;
    elseif strcmp(method,'intersect') || strcmp(method,'back')
        DISTS = (DISTSr(iai)+DISTSl(ibi))./2;
    elseif strcmp(method,'union')
        %[INDEX_PAIRS, iau, ibu] = union(INDEX_PAIRSr,INDEX_PAIRSl,'rows','sorted'); 
    end
end
end


addpath ~/software/flann/src/matlab/
addpath ../lib/utils
addpath ../lib/flann

siftfile = '~/databases/SF/SF-0/sf_0.sift.bin';

f = fopen(siftfile);

DESC = uint8(0);
DESC(128,149299425) = DESC;

chunk = 1;

for i=1:chunk:149299425

tmp = fread(f,[128 chunk]);
DESC(:,i:i+chunk-1) = uint8(tmp);
display(num2str(i/149299425));

end

fclose(f);

save '~/databases/SF/SF-0/sf_0.sift.mat' 'DESC'



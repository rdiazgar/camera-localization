function d=rootsift(d)
d = sqrt(single(d)) ;
%d = bsxfun(@times, d, 1./sqrt(sum(d.^2))) ;
d = d ./ repmat(sqrt(sum(d.^2)),[size(d,1) 1]);

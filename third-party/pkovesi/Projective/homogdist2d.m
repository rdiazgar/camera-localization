%----------------------------------------------------------------------
% Function to evaluate the symmetric transfer error of a homography with
% respect to a set of matched points as needed by RANSAC.

%function [inliers, H] = homogdist2d(H, x, t)
function [d, d1, d2] = homogdist2d(H, x)
%     if nargin < 3
%         t = 0.01;
%     end
    
    x1 = x(1:3,:);   % Extract x1 and x2 from x
    x2 = x(4:6,:);    
    
    % Calculate, in both directions, the transfered points    
    Hx1    = H*x1;
    invHx2 = H\x2;
    
    % Normalise so that the homogeneous scale parameter for all coordinates
    % is 1.
    
    x1     = hnormalise(x1);
    x2     = hnormalise(x2);     
    Hx1    = hnormalise(Hx1);
    invHx2 = hnormalise(invHx2); 
    
    d1 = sqrt(sum((x1-invHx2).^2)).^2;
    d2 = sqrt(sum((x2-Hx1).^2)).^2;
    d =  d1 + d2;
    %inliers = find(abs(d2) < t); 
    %inliers = abs(d2) < t;
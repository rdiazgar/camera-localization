function [cam, inliers, time] = localizeImage(data, th, focal)
% % script to register test images, using flann

if nargin < 3
	focal = 0;
end

Xi = data.X;
xi = data.x;

% RANSAC
info = imfinfo(data.path);

if focal
    K = diag([focal focal 1]);
    K(1,3) = info.Width/2;
    K(2,3) = info.Height/2;
    tic
    [R, t, inliers] = ransacPnP(K, Xi', xi', @kP3P, 3, th);
    time = toc;
else % do 4/6 pt algorithm
    xi_tmp = xi;
    xi_tmp(:,1) = xi(:,1) - info.Width/2;
    xi_tmp(:,2) = xi(:,2) - info.Height/2;
    %[P, inliers] = ransacfitcameramatrix(xi_tmp',Xi',th);
    %[K, R, t] = decomposecamerabundler(P,[1 1 1]);
    tic
    [P, inliers] = ransacfitcameramatrixP4Pf(xi_tmp',Xi',th);  
    time = toc;
    if isempty(P)
	cam = [];
	inliers = [];
	return;
    end 
    K = diag([P.f P.f 1]); K(1,3) = info.Width/2; K(2,3) = info.Height/2;
    R = P.R;
    t = P.t;
    focal = P.f;
end

%% Convert tu bundler format
% R(2:3,:) = -R(2:3,:);
% t(2:3) = -t(2:3);

if isempty(R)
        cam = [];
        inliers = [];
	return;
end

cam.f = focal;
cam.K = K;
cam.k = [0 0];
cam.R = R;
cam.t = t;
cam.c = [0 0];
cam.m = [1 1];

cam.direction = (R'*[0 0 1]')'; % not bundler format!
cam.position = (-R'*t)';
cam.path = data.path;


display(sprintf('Inliers: %u',numel(inliers)));

end

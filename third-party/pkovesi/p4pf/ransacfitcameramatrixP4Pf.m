% RANSACFITFUNDMATRIX - fits fundamental matrix using RANSAC
%
% Usage:   [F, inliers] = ransacfitfundmatrix(x1, x2, t)
%
% Arguments:
%          x1  - 2xN or 3xN set of homogeneous points.  If the data is
%                2xN it is assumed the homogeneous scale factor is 1.
%          x2  - 2xN or 3xN set of homogeneous points such that x1<->x2.
%          t   - The distance threshold between data point and the model
%                used to decide whether a point is an inlier or not. 
%                Note that point coordinates are normalised to that their
%                mean distance from the origin is sqrt(2).  The value of
%                t should be set relative to this, say in the range 
%                0.001 - 0.01  
%
% Note that it is assumed that the matching of x1 and x2 are putative and it
% is expected that a percentage of matches will be wrong.
%
% Returns:
%          F       - The 3x3 fundamental matrix such that x2'Fx1 = 0.
%          inliers - An array of indices of the elements of x1, x2 that were
%                    the inliers for the best model.
%
% See Also: RANSAC, FUNDMATRIX

% Copyright (c) 2004-2005 Peter Kovesi
% School of Computer Science & Software Engineering
% The University of Western Australia
% http://www.csse.uwa.edu.au/
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in 
% all copies or substantial portions of the Software.
%
% The Software is provided "as is", without warranty of any kind.

% February 2004  Original version
% August   2005  Distance error function changed to match changes in RANSAC

function [P, inliers, cost] = ransacfitcameramatrixP4Pf(x, X, t, feedback)

%     if ~all(size(x1)==size(x2))
%         error('Data sets x1 and x2 must have the same dimension');
%     end
    
    if nargin == 3
	feedback = 0;
    end
    
    [rowsx,nptsx] = size(x);
    [rowsX,nptsX] = size(X);
    if nptsx ~= nptsX
        error('x and X must have the same number of points');
    end
    if ~(((rowsx == 2) && (rowsX == 3)) || ((rowsx == 3) && (rowsX == 4)))
        error('x1 and x2 must have 2/3 rows or 3/4 rows respectively');
    end
    
    if rowsx == 2    % Pad data with homogeneous scale factor of 1
        x = [x; ones(1,nptsx)];
        X = [X; ones(1,nptsX)];        
    end
    
    % Normalise each set of points so that the origin is at centroid and
    % mean distance from origin is sqrt(2).  normalise2dpts also ensures the
    % scale parameter is 1.  Note that 'fundmatrix' will also call
    % 'normalise2dpts' but the code in 'ransac' that calls the distance
    % function will not - so it is best that we normalise beforehand.
    
    % need to do it for camera matrix?
%     [x, T1] = normalise2dpts(x);
%     [X, T2] = normalise3dpts(X);

    s = 4;  % Number of points needed to fit a fundamental matrix. Note that
            % only 7 are needed but the function 'fundmatrix' only
            % implements the 8-point solution.
    
    fittingfn = @P4Pfwrapper;
    distfn    = @P4Pfdist;
    degenfn   = @isdegenerate;
    % x1 and x2 are 'stacked' to create a 6xN array for ransac
    [P, inliers] = ransac([x; X], fittingfn, distfn, degenfn, s, t, feedback);
    inliers = find(inliers);

    %cost = camdist(P, [x; X]);

end
    

%--------------------------------------------------------------------------
% Function to evaluate the first order approximation of the geometric error
% (Sampson distance) of the fit of a fundamental matrix with respect to a
% set of matched points as needed by RANSAC.  See: Hartley and Zisserman,
% 'Multiple View Geometry in Computer Vision', page 270.
%
% Note that this code allows for F being a cell array of fundamental matrices of
% which we have to pick the best one. (A 7 point solution can return up to 3
% solutions)

% function d = camdist(P, x)
%     
%     x1 = x(1:3,:);    % Extract x1 and x2 from x
%     X = x(4:7,:);
%     
%     x_hat = P*X;
%     %valid = x_hat(3,:) < 0;
%     x_hat = x_hat./repmat(x_hat(3,:),[3 1]);
% 
%     d = sqrt(sum((x1 - x_hat).^2)) .^ 2;
% %     d(~valid) = Inf;
% %     
% %     [~, ~, ic] = unique(x1','rows','stable');
% %     IDX = accumarray(ic, 1:length(ic),[],@(x){x});
% %     ids = find(cellfun(@(x) length(x) > 1, IDX));
% %     for i=1:length(ids)
% %         d_i = d(IDX{ids(i)});
% %         [~, id] = min(d_i);
% %         %not_valid = ~ismember(1:length(d_i),id);
% %         d(IDX{ids(i)}((1:length(d_i)) ~= id)) = Inf;
% %     end
% 
% end
    	


%----------------------------------------------------------------------
% (Degenerate!) function to determine if a set of matched points will result
% in a degeneracy in the calculation of a fundamental matrix as needed by
% RANSAC.  This function assumes this cannot happen...
     
function r = isdegenerate(x)
    %r = 0;    
    x1 = x(1:3,:);    % Extract x1 and x2 from x
    X = x(4:7,:);
    
    x1u = unique(x1','rows');
    Xu = unique(X','rows');
    
    r = size(x1u,1) ~= size(Xu,1); 
end

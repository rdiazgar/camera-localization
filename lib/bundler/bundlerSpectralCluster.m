%% generate visibility list from bundler model.

topE = 50; % eigenvectors to use
k = 1000; % number of clusters

load ~/databases/eng-quad/colmap/bundler_export/bundle.db.out.gis.mat

A = full(bundlerconnectivitygraph(bundler));
A(~~eye(size(A))) = 0;
% get rid of unconnected cameras
camIdx = ~~(sum(A));
A = A(camIdx,:); A = A(:,camIdx);

D1 = diag(sum(A,2) .^ -.5);
%D2 = diag(sum(A,2));
%L = D2 - A;
L = D1 * A * D1;
%[U, ~] = eigs(L,topE);
[U,~,~] = svd(L);

% 

cIdx = kmeans(U(:,1:topE),k,'emptyaction','singleton');

out = sprintf('~/databases/eng-quad/colmap/bundler_export/SP_k%04u_e%03u',k,topE);
save(out,'camIdx','cIdx');



function [bestInl, bestM, bestD] = P4Pfdist(M, D, t)
   
    x = D(1:3,:);    % Extract x1 and x2 from x
    X = D(4:7,:);

    bestM = [];
    d = zeros(1,size(D,2));
    best = 0;
    for i=1:numel(M.f)
        P = diag([M.f(i) M.f(i) 1]) * [M.R(:,:,i) M.t(:,i)];
        x_hat = P*X;
        %valid = x_hat(3,:) < 0;
        x_hat = x_hat./repmat(x_hat(3,:),[3 1]);

        d = sqrt(sum((x - x_hat).^2));% .^ 2;

        bestInl = d < t;
        if sum(bestInl) > best
            best = sum(bestInl);
            bestD = d;
            bestM.f = M.f(i);
            bestM.R = M.R(:,:,i);
            bestM.t = M.t(:,i);
        end
    end
end
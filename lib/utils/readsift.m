function [DESC, POINTS] = readsift(sf)

fd = fopen(sf,'r');

parsed = textscan(fd,'%u %u \n',1);
npoints = parsed{1};

parsed = textscan(fd,'%f %f %f %f\n %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n %d %d %d %d %d %d %d %d\n',npoints);

POINTS = cell2mat(parsed(1:4))';
DESC = uint8(cell2mat(parsed(5:end)))';

fclose(fd);